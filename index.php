<?php require_once "./code.php";?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04: Access Modifiers and Inheritance</title>
</head>

<body>
    <h2>Building Variables</h2>

    <p>The name of the building is <?= $building->getName();?></p>
    <p>The <?= $building->getName();?> has <?= $building->getFloors();?> floors.</p>
    <p>The <?= $building->getName();?> has <?= $building->getAddress();?> floors.</p>

    <?php $building->setName('Caswym Complex'); ?>
    <p>The name of the building is <?= $building->getName();?></p>

    <h2>Condominium Variables</h2>

    <p>The name of the building is <?= $condominium->getName();?></p>
    <p>The <?= $condominium->getName();?> has <?= $condominium->getFloors();?> floors.</p>
    <p>The <?= $condominium->getName();?> has <?= $condominium->getAddress();?> floors.</p>

    <?php $condominium->setName('Enzo Tower'); ?>

    <p>The name of the condominium has been changed to <?= $condominium->getName(); ?></p>
</body>

</html>