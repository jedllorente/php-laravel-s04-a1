<?php


class Building {
	protected $name;
	protected $floors;
	protected $address;

	// Constructor is used during the creation of an object.
	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;

	}

    // !!!!!----WRONG SOLUION----!!!!!
    // 
    // 
    // public function printName() {
    //     return "The name of this building is $this->name";
    // }

    // public function printFloors() {
    //     return "The $this->name has $this->floors floors.";
    // }

    // public function printAddress() {
    //     return "The $this->name is location at $this->address.";
    // }
    // 
    // 
    // !!!!!----WRONG SOLUION----!!!!!/ 

    public function getFloors() {
        return $this->floors;
    }

    private function setFloors($floors) {
        $this->floors= $floors;
    }

    public function getAddress() {
        return $this->address;
    }

    private function setAddress($address) {
        $this->address= $address;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }
}

class Condominium extends Building{
	// Encapsulation - getter
	public function getName(){
		return $this->name;
	}

	// Encapsulation - setter
	public function setName($name){
		$this->name = $name;
	}
}


$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo' , '5' , 'Buendia Ave, Makati City, Philippines');